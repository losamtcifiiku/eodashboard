{
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        inherit (pkgs) stdenv lib;
      in rec {
        devShell = pkgs.mkShell {
          buildInputs = [ pkgs.nodePackages.pnpm pkgs.nodePackages.node2nix ];
        };

        defaultPackage = pkgs.stdenv.mkDerivation {
          name = "eod";
          src = ./.;

          buildPhase = let
            node_modules = (pkgs.callPackage ./. { }).shell.nodeDependencies;
          in ''
            ln -s ${node_modules}/lib/node_modules
            ${node_modules}/bin/eleventy
          '';

          installPhase = "cp -r _site/. $out";
        };
      });
}

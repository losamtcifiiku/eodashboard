const {DateTime} = require('luxon')

module.exports = (config) => {
  for (const layout of ["base", "post"]) {
    config.addLayoutAlias(layout, `${layout}.njk`);
  }

  config.addFilter("readableDate", dateObj => DateTime.fromJSDate(dateObj, { zone: 'utc' }).toFormat("dd LLL yyyy"));

  config.addFilter('htmlDateString', dateObj => DateTime.fromJSDate(dateObj, { zone: 'utc' }).toFormat('yyyy-LL-dd'));

  return { dir: { input: "src" } };
};
